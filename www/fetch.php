<?php
/** @noinspection PhpUnhandledExceptionInspection */
ini_set('display_errors', 'stdout');

$config = SimpleSAML\Configuration::getInstance();
$rconfig = SimpleSAML\Configuration::getOptionalConfig('config-refreshauthsources.php');

if(class_exists('SimpleSAML\Utils\Auth')) SimpleSAML\Utils\Auth::requireAdmin();
else if(class_exists('SimpleSAML_Utilities')) {
    /** @noinspection PhpUndefinedClassInspection */
    SimpleSAML_Utilities::requireAdmin();
}

SimpleSAML\Logger::setCaptureLog(TRUE);

$set = $rconfig;
SimpleSAML\Logger::info('[refreshAuthsources]: Executing.');
try {
	$expireAfter = $set->getInteger('expireAfter', NULL);
	if ($expireAfter !== NULL) {
		$expire = time() + $expireAfter;
	} else {
		$expire = NULL;
	}

	$outputDir = $set->getString('outputDir');
	$outputDir = $config->resolvePath($outputDir);

	if(!class_exists('SimpleSAML\Module\metarefresh\MetaLoader') && !class_exists('sspmod_metarefresh_MetaLoader')) {
		throw new Exception('Class sspmod_metarefresh_MetaLoader does not exist. Enable `metarefresh` module!');
	}
	$metaloader = new SimpleSAML\Module\metarefresh\MetaLoader($expire);
	if(is_callable(array($metaloader, 'setTypes'))) $metaloader->setTypes(array('saml20-sp-remote'));

	$source = $set->getArray('source');
    SimpleSAML\Logger::debug('[refreshAuthsources]: loading source.');
	$metaloader->loadSource($source);

	$metaloader->writeMetadataFiles($outputDir);
} catch (Exception $e) {
	$e = SimpleSAML\Error\Exception::fromException($e);
	$e->logWarning();
}

$logentries = SimpleSAML\Logger::getCapturedLog();

$t = new SimpleSAML\XHTML\Template($config, 'refreshAuthsources:fetch.tpl.php');
$t->data['logentries'] = $logentries;
$t->show();
