<?php /** @noinspection PhpUnhandledExceptionInspection */
/**
 * To use this test, uncomment menu item in hook_frontpage.php
 */
ini_set('display_errors', 'stdout');

$config = SimpleSAML\Configuration::getInstance();
$rconfig = SimpleSAML\Configuration::getOptionalConfig('config-refreshauthsources.php');

if(class_exists('SimpleSAML\Utils\Auth')) SimpleSAML\Utils\Auth::requireAdmin();
else if(class_exists('SimpleSAML_Utilities')) {
    /** @noinspection PhpUndefinedClassInspection */
    SimpleSAML_Utilities::requireAdmin();
}

SimpleSAML\Logger::setCaptureLog(TRUE);

$set = $rconfig;
SimpleSAML\Logger::info('[refreshAuthsources]: Testing.');
try {
	$expireAfter = $set->getInteger('expireAfter', NULL);
	if ($expireAfter !== NULL) {
		$expire = time() + $expireAfter;
	} else {
		$expire = NULL;
	}

    SimpleSAML\Logger::info('[refreshAuthsources]: Expire:'.date('Y-m-d H:i:s', $expire));

	$outputDir = $set->getString('outputDir');
	$outputDir = $config->resolvePath($outputDir);

    SimpleSAML\Logger::info('[refreshAuthsources]: outputDir:'.$outputDir);

	if(!class_exists('SimpleSAML\Module\metarefresh\MetaLoader') && !class_exists('sspmod_metarefresh_MetaLoader')) {
		throw new Exception('Class sspmod_metarefresh_MetaLoader does not exist. Enable `metarefresh` module!');
	}
	$metaloader = new SimpleSAML\Module\metarefresh\MetaLoader($expire);
	$metaloader->setTypes(array('saml20-sp-remote'));

	$source = $set->getArray('source');
    SimpleSAML\Logger::debug('[refreshAuthsources]: loading source.');
    SimpleSAML\Logger::info('[refreshAuthsources]: loading...:'.$source['src']);
	$metaloader->loadSource($source);

    SimpleSAML\Logger::info('[refreshAuthsources]: writing to...:'.$outputDir);

	$metaloader->writeMetadataFiles($outputDir);
    SimpleSAML\Logger::info('[refreshAuthsources]: Run cron job to complete the authsource generation.');
} catch (Exception $e) {
    SimpleSAML\Logger::info('[refreshAuthsources]: Exception.');
	$e = SimpleSAML\Error\Exception::fromException($e);
	$e->logWarning();
}

$logentries = SimpleSAML\Logger::getCapturedLog();

$t = new SimpleSAML\XHTML\Template($config, 'refreshAuthsources:fetch.tpl.php');
$t->data['logentries'] = $logentries;
$t->show();
