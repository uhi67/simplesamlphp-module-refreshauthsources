<?php

/*
	complement your config/authsources.php with following lines
	Replace sourcename for the name of your 
	(Create authsources/sourcename directory writable by www-data)
	(Keep existing $config array)
*/

$externalsourcefile = dirname(__FILE__).'/authsources/sourcename/authsources.php';
if(file_exists($externalsourcefile)) include $externalsourcefile;
else echo "<p>File $externalsourcefile is missing</p>";
 