<?php
/*
	Configuration template of the refreshauthsources module
	Copy this file into config directory
	Replace sourcename to the name of the actual datasource
	You may use only one source in the same time.
*/
$config = array(
	'cron'		=> array('hourly'),
	'source'	=> array(
		/*
		 * The url of XML metadata source. The URL must provide a metadata set valid only for this specific SP server. 
		 */
		'src' => 'http://metadataserver.dev/authsource/2',
		'certificates' => array(
			'current.crt',
		),
		'validateFingerprint' => '59:1D:4B:46:70:46:3E:ED:A9:1F:CC:81:6D:C0:AF:2A:09:2A:A8:01',
		'template' => array(
			'tags'	=> array('sourcename'),
			'authproc' => array(
				51 => array('class' => 'core:AttributeMap', 'oid2name'),
			),
		),
	),
	'expireAfter' 		=> 60*60*24*4, // Maximum 4 days cache time
	'outputDir' 	=> 'config/authsources/sourcename/',
	#'outputFileName' => 'authsources.php',	// The default is 'authsources.php', enable if you want different one.
	#'defaultEntry' => 'default-sp'; // The base entry for imported entries. The default is 'default-sp', enable if you want different one.
	/*
	 * Specify a RE pattern which matches to entityid and second submatch is the authsource identifier.
	*/
	'pattern' => '#https?://([\w.-]+)/simplesaml/module.php/saml/sp/metadata.php/([\w.-]+)#',
);
