<?php
/*
	This is a sample only.
	The module will generate similar file into your config/authsources/ directory
*/

$config['sampleservice'] = array(
        'saml:SP',
        'signature.algorithm' => 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256',
        
		'name'=>'Sample service',
		'description' => array (
	    	'en' => 'Sample service description for refreshAuthsources module',
	  	),
		'url' => 'http://sample.dev',
		'OrganizationName' => array (
		    'en' => 'Sample Organization',
		),
		'OrganizationDisplayName' => array (
		    'en' => 'Sample Organization',
		),
		'OrganizationURL' => array (
		    'en' => 'http://sample.dev',
		),
		'contacts' => array (
		   0 => array (
				'contactType' => 'technical',
				'surName' => 'Uherkovich Péter',
				'emailAddress' => array (
					0 => 'uherkovich.peter@pte.hu',
				),
		   ),
		),
		'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
		'attributes' => array (
			0 => 'urn:oid:1.3.6.1.4.1.5923.1.1.1.6',
			1 => 'urn:oid:2.16.840.1.113730.3.1.241',
		    2 => 'urn:oid:1.3.6.1.4.1.5923.1.1.1.10',
		),
		'attributes.required' => array (
			0 => 'urn:oid:1.3.6.1.4.1.5923.1.1.1.6',
			1 => 'urn:oid:2.16.840.1.113730.3.1.241',
		    2 => 'urn:oid:1.3.6.1.4.1.5923.1.1.1.10',
		),
		'attributes.NameFormat' => 'urn:oasis:names:tc:SAML:2.0:attrname-format:uri',
	    'errorUrl' => 'http://sample.dev',
	    'UIInfo' => array(
	        'DisplayName' => array(
	        	'en' => 'Sample Service',
	        ),
	        'Description' => array(
	        	'en' => 'Sample service description for refreshAuthsources module',
	        ),
	        'PrivacyStatementURL' => array (
	            'en' => 'http://sample.dev/privacy',
	        ),
	        'Logo' => array(
	            0 => array(
	                'url' => 'http://sample.dev/img/logo.png',
	                'height' => 64,
	                'width' => 64,
	            ),
	        ),
			'Keywords'=> array(
				'en' => array('sample', 'garden', 'tap'),
			)
	    ),
);
