refreshAuthsources module
=========================

A garden-tap simple module for SimpleSAMLphp that downloads and refreshes SP metadata information from a private resource registry for authsources.php.

Version 1.2.1 (Release 2019-12-10)

## Installation ##

Once you have installed SimpleSAMLphp, installing this module is very simple. Just copy the files or clone the repository into the `modules/refreshAuthsources directory`
inside your SimpleSAMLphp installation.

The module is disabled by default. If you want to enable the module once installed, you just need to create a file named enable in the `modules/refreshAuthsources/`
directory.

## Requirements, configuration ##

SimpleSAMLphp v1.13 or newer required.

Your private, external resource registry must provide a SAML v2.0 conform (see "urn:oasis:names:tc:SAML:2.0:metadata" namespace) metadata file,
which contains only SPSSODescriptor type EntityDescriptor records for this authsource.

Complement your config/authsources.php file using template in `modules/refreshAuthsources/config-templates/authsources.php`

Copy `modules/refreshAuthsources/config-templates/config-refreshauthsources.php` to your `config/` directory, and fill in data of your environment.

Enable `metarefresh` module.
Enable and configure *cron* module to run hourly.

## Required permissions in file system ##

/etc/simplesamlphp/authsources/ -- writable by www-data
/usr/share/simplesamlphp/modules/refreshAuthsources/ -- readable by www-data

## Version history

### 1.2.1
- class check order to avoid warning in simplesamlphp log

### 1.2
- SimpleSAMLphp v1.17 compatible

### 1.1
- SimpleSAMLphp v1.14 compatible

### 1.0
- SimpleSAMLphp v1.12 compatible
