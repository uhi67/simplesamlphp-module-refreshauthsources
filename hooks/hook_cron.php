<?php /** @noinspection PhpUnused */
/**
 * Hook to run a cron job.
 *
 * @param array &$croninfo  Output
 */
function refreshAuthsources_hook_cron(&$croninfo) {
	assert(is_array($croninfo));
	assert(array_key_exists("summary", $croninfo));
	assert(array_key_exists("tag", $croninfo));

	SimpleSAML\Logger::info('cron [refreshAuthsources]: Running cron in cron tag [' . $croninfo['tag'] . '] ');

	/** @var array $keys -- array of keys we will copy from metadata to authsource */
	$keys = array(
		'name',
		'description',
		'OrganizationName',
		'OrganizationDisplayName',
		'OrganizationURL',
		'contacts',
		'NameIDFormat',
		'attributes',
		'attributes.requried',
		'attributes.NameFormat',
		'UIInfo',
	 );

	try {
		$config = SimpleSAML\Configuration::getInstance();
		$rconfig = SimpleSAML\Configuration::getOptionalConfig('config-refreshauthsources.php');

		$set = $rconfig;
		$cronTags = $set->getArray('cron');
		if (in_array($croninfo['tag'], $cronTags)) {
			SimpleSAML\Logger::info('cron [refreshAuthsources]: Executing.');

			$expireAfter = $set->getInteger('expireAfter', NULL);
			if ($expireAfter !== NULL) {
				$expire = time() + $expireAfter;
			} else {
				$expire = NULL;
			}

			$outputDir = $set->getString('outputDir');
			$outputDir = $config->resolvePath($outputDir);
			if(!file_exists($outputDir)) {
				$croninfo['summary'][] = sprintf('cron [refreshAuthsources]: Directory `%s not found, attempting to create.', $outputDir);
				mkdir($outputDir);
			}

			$metaloader = new SimpleSAML\Module\metarefresh\MetaLoader($expire);
			if(is_callable($metaloader, 'setTypes')) $metaloader->setTypes(array('saml20-sp-remote'));

			$source = $set->getArray('source');
			SimpleSAML\Logger::debug($info = sprintf("cron [refreshAuthsources]: loading source `%s`", $source['src']));
			$croninfo['summary'][] = $info;
			$metaloader->loadSource($source);

			$metaloader->writeMetadataFiles($outputDir);


			$authsourcefilename = $rconfig->getString('outputFileName', 'authsources.php');

			// Reading imported data into $metadata array
			$metadatafile = $outputDir.'/saml20-sp-remote.php';
			if(!file_exists($metadatafile)) $croninfo['summary'][] = sprintf('cron [refreshAuthsources]: Error: `%s` file is not found.', $metadatafile);
			else {
				$metadata = array();
                /** @noinspection PhpIncludeInspection */
                include $metadatafile;

				// Preparing authsources array
				$authsourcepath = $config->resolvePath('config/authsources.php');
				#$croninfo['summary'][] = sprintf('Using defaults from file `%s`.', $authsourcepath);
                /** @var array $config */
                /** @noinspection PhpIncludeInspection */
                include $authsourcepath; // creates $config array
				$authsources = $config;

				// Delete included previously generated elements
				foreach(array_keys($authsources) as $authsourcename) {
					if(isset($authsources[$authsourcename]['metadataSource']) && $authsources[$authsourcename]['metadataSource'] == $source['src'])
						unset($authsources[$authsourcename]);
				}

				// Determining template entry. Default template is configured in config, if does not exist, default-sp will be used, or empty template if it is not found even
				$defaultEntry = $rconfig->getString('defaultEntry', 'default-sp');
				$defaultTemplate = array();
				if(isset($authsources[$defaultEntry])) $defaultTemplate = $authsources[$defaultEntry];
				else {
					$croninfo['summary'][] = sprintf('cron [refreshAuthsources]: Default template `%s` does not exists.', $defaultEntry);
					if($defaultEntry!='default-sp') {
						if(isset($authsources['default-sp'])) $defaultTemplate = $authsources['default-sp'];
					}
				}

		 		$pattern = $rconfig->getString('pattern', '#https?://([\w.-]+)/simplesaml/module.php/saml/sp/metadata.php/([\w.-]+)#');
		 		$externalsources = array();
				foreach($metadata as $entity=>$data) {
					$entityid = $data['entityid'];

					// compute authsourcename from entityid
                    $authsourcename = '';
					$matches = array();
					if(preg_match($pattern, $entityid, $matches)) {
						$authsourcename = $matches[2];
						$croninfo['summary'][] = "cron [refreshAuthsources]: Loading entity `$entityid`";
					}

					// Skip if exists in authsources
					if(isset($authsources[$authsourcename])) {
						$croninfo['summary'][] = sprintf('Authsource `%s` exists, skipping.', $authsourcename);
						continue;
					}

					// Skip if already exists in $externalsources
					if(isset($externalsources[$authsourcename])) {
						$croninfo['summary'][] = sprintf('cron [refreshAuthsources]: Authsource `%s` already exists in this set, skipping.', $authsourcename);
						continue;
					}

					// Using template defined in metadata if exists
					$template = $defaultTemplate;
					$templateName = isset($data['authsourceTemplate']) ? $data['authsourceTemplate'] : false;
					if($templateName && isset($authsources[$templateName])) $template = $authsources[$templateName];

					$authsource = $template;
		 			$authsource['entityID'] = $entityid;
		 			$authsource['metadataSource'] = $source['src'];
		 			foreach($keys as $key) {
		 				if(isset($data[$key])) $authsource[$key] = $data[$key];

		 			}
		 			$externalsources[$authsourcename] = $authsource;
		 		}

		 		// Export $externalsources array to $outputDir/$authsourcefilename
				$content  = '<?php' . "\n" . '/* This file was generated by the refreshAuthsources module at '. date(DATE_ATOM) . "\n";
				$content .= ' Do not update it manually as it will get overwritten' . "\n" . '*/' . "\n";
				foreach($externalsources as $authsourcename => $m) {
					$content .= "\n";
					$content .= '$config[\'' . $authsourcename . '\'] = ' . var_export($m, TRUE) . ';' . "\n";
				}
				$content .= "\n" . '?>';
				$filename = $outputDir.'/'.$authsourcefilename;

				if(is_callable(array('SimpleSAML\Utils\System', 'writeFile')))
					SimpleSAML\Utils\System::writeFile($filename, $content, 0644);
				else if(is_callable(array('SimpleSAML_Utilities', 'writeFile')))
					/** @noinspection PhpDeprecationInspection */ // compatibility with older SimplesamlPHP versions
					SimpleSAML\Utilities::writeFile($filename, $content, 0644);
				else
					throw new Exception('No writeFile method found');
			}
		}
	} catch (Exception $e) {
		$croninfo['summary'][] = 'cron [refreshAuthsources]: Error: ' . $e->getMessage();
	}
}
