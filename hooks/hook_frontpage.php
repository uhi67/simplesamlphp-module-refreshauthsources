<?php
/**
 * Hook to add links to the frontpage.
 *
 * @param array &$links  The links on the frontpage, split into sections.
 */
function refreshAuthsources_hook_frontpage(&$links) {
	assert(is_array($links));
	assert(array_key_exists("links", $links));

	$links['config'][] = array(
		'href' => SimpleSAML\Module::getModuleURL('refreshAuthsources/fetch.php'),
		'text' => array('en' => 'refreshAuthsources: fetch metadata'),
	);
	// Test script. Uncomment to use from admin page.
//	$links['config'][] = array(
//		'href' => SimpleSAML\Module::getModuleURL('refreshAuthsources/test.php'),
//		'text' => array('en' => 'refreshAuthsources: self test'),
//	);

}
